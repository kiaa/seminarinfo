<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

//category
Route::get('/category','category\CategoryController@index');
Route::post('/category/store','category\CategoryController@store');
Route::get('/search_category','category\CategoryController@search_category')-> name('search_category');
Route::get('/category/{id}/edit','category\CategoryController@edit');
Route::post('/category/{id}/update','category\CategoryController@update');
Route::get('/category/{id}/delete','category\CategoryController@delete');

//event
Route::get('/event','Event\EventController@index');
Route::post('/event/store','Event\EventController@store');
Route::get('/search_event','Event\EventController@search_event')-> name('search_event');
Route::get('/event/{id}/edit','Event\EventController@edit');
Route::post('/event/{id}/update','Event\EventController@update');
Route::get('/event/{id}/delete','Event\EventController@delete');
Route::get('/event/{id}','Event\EventController@detail');
