<?php

namespace App\Http\Controllers\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
     public function index()
    {
        $category = Category::orderBy('created_at','desc')->paginate(2);
      
        return view('category.index',compact('category'));
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name'    => 'required',
    	]);

    	try {
    	$category = new Category();
    	$category->name = $request->name;
 
        $category->save();

    	Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		session::flash('message','Data Gagal Disimpan');
    		return redirect()->back();
    	}
    }
    public function search_category (Request $request)
        {
            $category = Category::query();

            if (request()->has("search") && strlen(request()->query("search"))>=1){
                $category -> where("name","like","%" . request()->query ("search") . "%" );
            }
            $Pagination = 2;
            $category = $category->orderBy('created_at','desc') ->paginate($Pagination);

            return view('category.index', compact('category'));
        }

    public function edit ($id)
    {
        $category = Category::find($id);

        return view('category.edit',compact('category'));
    }   

    public function update (Request $request, $id)
    	{
    		$category = Category::find($id);
            $category ->name = $request ->name;

            $category->save();

            Session::flash('message','Yeah! Anda berhasil Update');
        return redirect()->back();

    	}
    public function delete ($id)
    		{
    		   $category = Category::find($id);
    		   $category->delete();

    		   Session::flash('message','Anda Berhasil menghapus!');
               return redirect()->back();
    		}	
}
