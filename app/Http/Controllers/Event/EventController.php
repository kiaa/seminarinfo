<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use Illuminate\Support\Facades\Session;

class EventController extends Controller
{
     public function index()
    {
        $Event = Event::orderBy('created_at','desc')->paginate(2);
      
        return view('Event.index',compact('Event'));
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name'    => 'required',
    		'image'  => 'required | image | mimes:jpg,jpeg,png,gif',
    		'Registration'    => 'required',
    		'Description'  => 'required'
    	]);

    	try {
    	$imageName = time().'.'.request()->image->getClientOriginalExtension();
    	request()->image->move(public_path('images'), $imageName);

    	$Event = new Event();
    	$Event->name   = $request->name;
    	$Event->image = $imageName;
    	$Event->Description = $request->Description;
    	$Event->Registration = $request->Registration;
        $Event->save();

    	Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		session::flash('message','Data Gagal Disimpan');
    		return redirect()->back();
    	}
    }
    public function search_event (Request $request)
        {
            $Event = Event::query();

            if (request()->has("search") && strlen(request()->query("search"))>=1){
                $Event -> where("name","like","%" . request()->query ("search") . "%" );
            }
            $Pagination = 2;
            $Event = $Event->orderBy('created_at','desc') ->paginate($Pagination);

            return view('Event.index', compact('Event'));
        }

     public function edit ($id)
    {
        $Event = Event::find($id);
        return view('Event.edit',compact('Event'));
    }   

    public function update (Request $request, $id)
    	{

    		$Event = Event::find($id);
            $Event ->name = $request ->name;
            $Event ->image = 'request | image | mimes:jpg,jpeg,png,gif';
            $Event ->Description = $request ->Description;
            $Event ->Registration = $request ->Registration;

            $Event->save();

            Session::flash('message','Yeah! Anda berhasil Update');
       
    		 return redirect()->back();
    	    
    	}

    public function delete ($id)
    {
      $Event = Event::find($id);
      $Event->delete();

    Session::flash('message','Anda Berhasil menghapus!');
            return redirect()->back();
    }

    public function detail ($id)
    {
        $Event = Event::find($id);

        return view('Event.detail', compact('Event'));
    }	   	
}
