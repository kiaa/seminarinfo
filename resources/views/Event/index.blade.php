@include('base.header')

<div class="content-wrapper">
  <section class="content-header">
    <h1>Event</h1>
  </section>

  <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Event</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('message'))
            <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form action="/event/store" method="post" enctype="multipart/form-data">
              @csrf

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              <div class="box-body">
                <div class="form-group">
                  <label >name</label>
                  <input class="form-control" name="name" placeholder="Enter Name">
                </div>

                <div class="form-group">
                  <label >image</label>
                  <input type="file" name="image" class="form-control">
                </div>

                 <div class="form-group">
                  <label>Registration</label>
                  <input class="form-control" name="Registration" placeholder="Enter registration">
                </div>

                <div class="form-group">
                  <label>Description</label>
                  <input class="form-control" name="Description" placeholder="Enter description">
                </div>
              </div>
 
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Event Table</h3>

              <div class="box-tools">
                <form action="{{ route('search_event') }}" method="GET" class="form-group">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </form>
            </div>

            <form action="/event" method="GET">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Registration</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                @foreach($Event as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->name }}</td>
                  <td><img src="/images/{{ $item->image }}" style="width: 50px; height: 40px"></td>
                  <td>{{ $item->Registration}}</td>
                  <td>{{ $item->Description}}</td>
                    <td>
                    <a class="btn btn-success btn-sm" href="/event/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger btn-sm" href="/event/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/event/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <div class="text-center">
              {{ $Event->links() }}
            </div>
          </form>
        </div>

        </div>
      </div>
    </section>
</div>

@include('base.footer')