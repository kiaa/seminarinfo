 @include('base.header')
<div class="content-wrapper">
  <section class="content-header">
    <h1>Event</h1>
  </section>

  <section class="content">
    <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Event </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
             
              <form role="form" action="/event/{{$Event->id}}" method="get" enctype="multipart/from-data">

                @csrf
                @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul> 
              </div>
              @endif

              <table class="table table-bordered">
                <tr>
                  <td>Id</td>
                  <td> {{ $Event->id }} </td>
                </tr>

                <tr>
                  <td>name</td>
                  <td> {{ $Event->name }} </td>
                </tr>

                <tr>
                  <td>Description</td>
                  <td> {{ $Event->Description }} </td>
                </tr>

                 <tr>
                  <td>Registration</td>
                  <td> {{ $Event->Registration }} </td>
                </tr>

                 <tr>
                  <td>image</td>
                  <td> {{ $Event->image}} </td>
                </tr>
              </table>

              <div class="form-group">
                <a class="btn btn-warning" href="/event">Back</a>
              </div>
              </form>

            </div>
          </div>
  </section>
</div>
@include('base.footer')