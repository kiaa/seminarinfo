@include('base.header')
<div class="content-wrapper">

	<section class="content-header">
		<h1>Event</h1>
	</section>

	<section class="content">
		<div class="box box-warning">

            <div class="box-header with-border">
              <h3 class="box-title">Tambah Event</h3>
            </div>
            
            <div class="box-body">
              @if(Session::has('message'))
            <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
             
              <form role="form" action="/event/{{$Event->id}}/update" method="post" enctype="multipart/from-data">

                @csrf
                @if (count($errors) > 0)
              
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul> 
              </div>
              @endif

              <form role="form">
                <div class="form-group">
                  <label >name</label>
                  <input type="text" class="form-control" name="name" value="{{ $Event->name}}">
                </div>

                <div class="form-group">
                  <label >image</label>
                  <input type="file" class="form-control" name="image" value="{{ $Event->image}}">
                </div>

                 <div class="form-group">
                  <label>Registration</label>
                  <input type="text" class="form-control" name="Registration" value="{{ $Event->Registration}}">
                </div>

                <div class="form-group">
                  <label>Description</label>
                  <input type="text" class="form-control" name="Description" value="{{ $Event->Description}}">
                </div>

                  <div class="form-group">
                <input class="btn btn-primary" type="submit" value="update"></input>
                <a class="btn btn-warning" href="/event">Back</a>
              </div>

              </form>
            </div>
          </div>
	</section>
</div>
@include('base.footer')