@include('base.header')
<div class="content-wrapper">

	<section class="content-header">
		<h1>Category</h1>
	</section>

	<section class="content">
		<div class="box box-warning">

            <div class="box-header with-border">
              <h3 class="box-title">Tambah Category</h3>
            </div>
            
            <div class="box-body">
              @if(Session::has('message'))
            <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
             
              <form role="form" action="/category/{{$category->id}}/update" method="post" enctype="multipart/from-data">

                @csrf
                @if (count($errors) > 0)
              
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul> 
              </div>
              @endif

              <form role="form">
                <div class="form-group">
                  <label>Nama Category</label>
                  <input type="text" class="form-control" name="name" value="{{ $category->name}}">
                </div>

                  <div class="form-group">
                <input class="btn btn-primary" type="submit" value="update"></input>
                <a class="btn btn-warning" href="/category">Back</a>
              </div>

              </form>
            </div>
          </div>
	</section>
</div>
@include('base.footer')