@include('base.header')
<div class="content-wrapper">

	<section class="content-header">
		<h1>Category</h1>
	</section>

	<section class="content">
		<div class="box box-warning">

            <div class="box-header with-border">
              <h3 class="box-title">Tambah Category</h3>
            </div>
            
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
             
              <form action="/category/store" method="POST">
                @csrf
                @if (count($errors) > 0)              
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul> 
              </div>
              @endif

              <form role="form">
                <div class="form-group">
                  <label>Nama Category</label>
                  <input type="text" class="form-control" name="name" placeholder="Isikan category...">
                </div>
                <input type="submit" value="Tambah">
              </form>
            </div>
          </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <div class="box-header">
              <h3 class="box-title">Category Table</h3>
              <div class="box-tools">

               <div class="box-tools">
                <form action="{{ route('search_category') }}" method="GET" class="form-group">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form action="/category" method="GET">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                @foreach($category as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->name }}</td>
                  <td>
                    <a class="btn btn-success btn-sm" href="/category/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger  btn-sm" href="/category/{{$item->id}}/delete">Delete</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>

            <div>
            </div class="text-center">
            {{$category->links() }}
          </div>
          </form>
          
        </div>
      </div>
	</section>
</div>
@include('base.footer')